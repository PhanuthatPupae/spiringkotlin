package dv.spring592110131.demo.entity

import sun.text.bidi.BidiLine
import javax.persistence.*

@Entity
data class Customer(
        override  var name:String? = null,
        override  var email: String? = null,
        override var userStatus: UserStatus?= UserStatus.PENDING
):User {
    @Id
    @GeneratedValue
    var id:Long? = null

    @OneToOne
    var billingAddress: Address?=null
    @OneToOne
    var defaultAddress: Address?=null
    @OneToMany
    var shippingAddress = mutableListOf<Address>()

}