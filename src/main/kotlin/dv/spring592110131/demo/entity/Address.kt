package dv.spring592110131.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class  Address (

        var homeAddress: String?=null,
        var subdistrict : String?=null,
        var province : String?=null,
        var postCode:String?=null
){
        @Id
        @GeneratedValue
        var id:Long? = null
}