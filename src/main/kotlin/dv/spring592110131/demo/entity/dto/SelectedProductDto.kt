package dv.spring592110131.demo.entity.dto

data class SelectedProductDto(var quantity :Int?=null,
                              var product : ProductDto?=null)