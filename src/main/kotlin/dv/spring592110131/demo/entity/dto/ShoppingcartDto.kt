package dv.spring592110131.demo.entity.dto

import dv.spring592110131.demo.entity.Address
import dv.spring592110131.demo.entity.Customer
import dv.spring592110131.demo.entity.SelectedProduct
import dv.spring592110131.demo.entity.ShoppingCartStatus

data class ShoppingcartDto(var shoppingCartStatus: ShoppingCartStatus?=null,
                           var Customer: Customer?=null,
                           var shippingAddress: Address?=null,
                           var SelectedProduct : List<SelectedProductDto>?= mutableListOf<SelectedProductDto>())