package dv.spring592110131.demo.entity.dto

import org.apache.commons.math3.stat.descriptive.summary.Product

data class PageProductDto (var totalPage:Int?=null,
                           var totalElement: Long?=null,
                           var products: List<ProductDto> = mutableListOf())
