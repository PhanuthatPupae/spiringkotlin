package dv.spring592110131.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Admin (
        override var name : String? = null,
        override  var email: String?=null,
        override var userStatus: UserStatus? = UserStatus.PENDING
):User{
    @Id
    @GeneratedValue
    var id:Long? = null
}