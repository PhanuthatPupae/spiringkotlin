package dv.spring592110131.demo.repository


import dv.spring592110131.demo.entity.Address
import org.springframework.data.repository.CrudRepository

interface AddressRepository: CrudRepository<Address,Long>