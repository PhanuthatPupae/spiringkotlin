package dv.spring592110131.demo.repository

import dv.spring592110131.demo.entity.Customer
import org.springframework.data.repository.CrudRepository

interface CustomerRepository : CrudRepository<Customer, Long> {
    fun findByName(name: String): Customer
    fun findByNameContaining(name: String): List<Customer>
    fun findByNameEndingWith(name: Any): List<Customer>
    fun findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name: String, email: String): List<Customer>
    fun findByDefaultAddress_ProvinceContainingIgnoreCase(name: String):List<Customer>
}