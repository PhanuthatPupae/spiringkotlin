package dv.spring592110131.demo.dao

import dv.spring592110131.demo.entity.Product
import dv.spring592110131.demo.repository.ProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ProductDaoDBImpl: ProductDao{
    override fun findById(id: Long): Product? {
        return productRepository.findById(id).orElse(null)
    }

    override fun save(product: Product): Product {
        return productRepository.save(product)
    }


    override fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<Product> {
        return productRepository.findByNameContainingIgnoreCase(name,PageRequest.of(page,pageSize))
    }

    override fun getProductByManuName(name: String): List<Product> {
        return productRepository.findByManufacturer_NameContainingIgnoreCase(name)
    }



    override fun getProductByPartialNameAndDesc(name: String, desc: String): List<Product> {
        return productRepository.findByNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(name, desc)
    }

    override fun getProductByPartialName(name: String): List<Product> {
        return productRepository.findByNameContaining(name)
    }

    @Autowired
    lateinit var productRepository: ProductRepository

    override fun getProduct(): List<Product> {
        return productRepository.findAll().filterIsInstance(Product::class.java)
    }

    override fun getProductByName(name: String): Product? {
        return productRepository.findByName(name)
    }
}