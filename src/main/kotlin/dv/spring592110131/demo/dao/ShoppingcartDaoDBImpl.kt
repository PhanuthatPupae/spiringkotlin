package dv.spring592110131.demo.dao

import dv.spring592110131.demo.entity.ShoppingCart
import dv.spring592110131.demo.repository.ShoppingCartRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ShoppingcartDaoDBImpl:ShoppingcartDao{
    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository

    override fun getShoppingcarts(): List<ShoppingCart> {
        return shoppingCartRepository.findAll().filterIsInstance(ShoppingCart::class.java)
    }
}