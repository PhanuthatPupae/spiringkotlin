package dv.spring592110131.demo.dao

import dv.spring592110131.demo.entity.Customer
import javax.validation.constraints.Email

interface CustomerDao {


    fun getCustomers(): List<Customer>
    fun getCustomerByName(name:String):Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomersEnd(name: String): List<Customer>
    fun getProductByPartialNameAndEmil(name:String,email: String):List<Customer>
    fun getCustomerByProvince(name: String):List<Customer>
    fun save(customer: Customer):Customer

}