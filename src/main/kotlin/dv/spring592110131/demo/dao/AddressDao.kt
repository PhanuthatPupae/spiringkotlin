package dv.spring592110131.demo.dao

import dv.spring592110131.demo.entity.Address
import dv.spring592110131.demo.entity.Manufacturer

interface AddressDao{
    fun save(address: Address): Address
    fun getAddress():List<Address>
    fun findById(addId: Long): Address?

}