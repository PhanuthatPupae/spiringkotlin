package dv.spring592110131.demo.dao

import dv.spring592110131.demo.entity.Manufacturer

import dv.spring592110131.demo.repository.ManufacturerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import javax.persistence.Id

@Profile("db")
@Repository

class ManufacturerDaoDBImpl : ManufacturerDao{
    override fun findById(id: Long): Manufacturer? {
        return manufacturerRepository.findById(id).orElse(null)
    }

    override fun getMamufacturers(): List<Manufacturer> {
        return manufacturerRepository.findAll().filterIsInstance(Manufacturer::class.java)
    }

    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    override fun save(manufacturer: Manufacturer): Manufacturer {
        return manufacturerRepository.save(manufacturer)
    }
}