package dv.spring592110131.demo.dao

import dv.spring592110131.demo.entity.Address
import dv.spring592110131.demo.repository.AddressRepository
import dv.spring592110131.demo.repository.ManufacturerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class AddressDaoDBImpl:AddressDao {
    override fun findById(addId: Long): Address? {
        return addressRepository.findById(addId).orElse(null)
    }

    override fun getAddress(): List<Address> {
        return addressRepository.findAll().filterIsInstance(Address::class.java)
    }

    @Autowired
    lateinit var addressRepository: AddressRepository
    override fun save(address: Address): Address {
        return addressRepository.save(address)
    }

}