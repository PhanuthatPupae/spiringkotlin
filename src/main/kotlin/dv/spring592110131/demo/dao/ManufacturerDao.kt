package dv.spring592110131.demo.dao

import dv.spring592110131.demo.entity.Manufacturer

interface ManufacturerDao {
    fun getMamufacturers(): List<Manufacturer>
    fun save(manufacturer: Manufacturer):Manufacturer
    fun findById(manuId: Long): Manufacturer?

}