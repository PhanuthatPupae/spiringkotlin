package dv.spring592110131.demo.service

import dv.spring592110131.demo.dao.AddressDao
import dv.spring592110131.demo.entity.Address
import dv.spring592110131.demo.entity.dto.AddressDto
import dv.spring592110131.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AddressServiceImpl:AddressService{
    override fun getAddress(): List<Address> {
        return addressDao.getAddress()
    }

    @Autowired
    lateinit var addressDao: AddressDao


    override fun save(address: AddressDto): Address {
        val address = MapperUtil.INSTANCE.mapAddress(address)
        return  addressDao.save(address)
    }

}