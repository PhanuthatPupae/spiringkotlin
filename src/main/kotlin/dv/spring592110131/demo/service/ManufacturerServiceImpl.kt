package dv.spring592110131.demo.service

import dv.spring592110131.demo.dao.ManufacturerDao
import dv.spring592110131.demo.entity.Manufacturer
import dv.spring592110131.demo.entity.dto.ManufacturerDto
import dv.spring592110131.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service

class ManufacturerServiceImpl:ManufacturerService{
    override fun save(manu: ManufacturerDto): Manufacturer {
     val manufacturer=MapperUtil.INSTANCE.mapManufacturer(manu)
        return manufacturerDao.save(manufacturer)
    }

    @Autowired
    lateinit var manufacturerDao: ManufacturerDao
    override fun getManufacturers(): List<Manufacturer> {
        return manufacturerDao.getMamufacturers()
    }
}