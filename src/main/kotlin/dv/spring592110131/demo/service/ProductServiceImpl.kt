package dv.spring592110131.demo.service

import dv.spring592110131.demo.dao.ManufacturerDao
import dv.spring592110131.demo.dao.ProductDao
import dv.spring592110131.demo.entity.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class ProductServiceImpl : ProductService {
    override fun remove(id: Long): Product? {
        val product=productdao.findById(id)
        product?.isDeleted =true
        return product
    }

    @Autowired
    lateinit var manufacturerDao: ManufacturerDao

    @Transactional
    override fun save(manuId: Long, product: Product): Product {
        val manufacturer = manufacturerDao.findById(manuId)
        val product = productdao.save(product)
        product.manufacturer = manufacturer
        manufacturer?.products?.add(product)


        return product
    }

    override fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<Product> {
        return productdao.getProductWithPage(name, page, pageSize)
    }

    override fun getProductByManuName(name: String): List<Product> {
        return productdao.getProductByManuName(name)
    }

    override fun getProductByPartialNameDes(name: String, desc: String): List<Product> {
        return productdao.getProductByPartialNameAndDesc(name, desc)
    }

    override fun getProductByPartialName(name: String): List<Product> {
        return productdao.getProductByPartialName(name)
    }

    @Autowired
    lateinit var productdao: ProductDao

    override fun getProduct(): List<Product> {
        return productdao.getProduct()
    }

    override fun getProductByname(name: String): Product? = productdao.getProductByName(name)


}