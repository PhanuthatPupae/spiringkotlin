package dv.spring592110131.demo.service

import dv.spring592110131.demo.entity.Address
import dv.spring592110131.demo.entity.dto.AddressDto


interface AddressService{
    fun getAddress():List<Address>
    fun save(address: AddressDto):Address
}