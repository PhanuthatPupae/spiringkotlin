package dv.spring592110131.demo.service

import dv.spring592110131.demo.dao.ShoppingcartDao
import dv.spring592110131.demo.entity.ShoppingCart
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class ShoppingcartServiceImpl :ShoppingcartService {
    @Autowired
    lateinit var shoppingcartDao: ShoppingcartDao

    override fun getShoppingcart(): List<ShoppingCart> {
        return shoppingcartDao.getShoppingcarts()
    }

}