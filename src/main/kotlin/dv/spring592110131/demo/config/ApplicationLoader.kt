package dv.spring592110131.demo.config

import dv.spring592110131.demo.entity.*
import dv.spring592110131.demo.repository.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import javax.transaction.Transactional



@Component
class ApplicationLoader : ApplicationRunner {
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    @Autowired
    lateinit var productRepository: ProductRepository
    @Autowired
    lateinit var addressRepository: AddressRepository
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository
    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository
    @Autowired
    lateinit var dataLoader: DataLoader




    @Transactional
    override fun run(args: ApplicationArguments?) {



        var manul = manufacturerRepository.save(Manufacturer("CAMT", "0000000"))
        val manu2 = manufacturerRepository.save(Manufacturer("Apple", "053123456"))
        val manu3 = manufacturerRepository.save(Manufacturer("SAMSUNG", "555666777888"))
        var productl = productRepository.save(Product("CAMT", "The best College in CMU", 0.0, 1,
                "http://www.camt.cmu.ac.th/th/images/logo/jpg"))
        var product2 = productRepository.save(Product("iPhone", "It's a phone", 28000.00, 20, "https://www.jaymartstore.com/Products/iPhone-X-64GB-Space-Grey--1140900010552--4724"))
        var product3 = productRepository.save(Product("Note 9", "Other Iphone", 28001.00, 10, Manufacturer("Samsung", "555666777888"), "http://dynamic-cdn.eggdigital.com/e56zBiUt1.jpg"))
        var product4 = productRepository.save(Product("Prayuth", "The best PM ever", 1.00, 1, Manufacturer("CAMT", "0000000"), "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Prayut_Chan-o-cha_%28cropped%29_2016.jpg/200px-Prayut_Chan-o-cha_%28cropped%29_2016.jpg"))



        manul.products.add(productl)
        manul.products.add(product2)
        manul.products.add(product3)
        manul.products.add(product4)


        productl.manufacturer = manul
        product2.manufacturer = manul
        product3.manufacturer = manul
        product4.manufacturer = manul

        var addressl = addressRepository.save(Address("ถนนอนุสาวรีย์ประชาธิปไตย", "แขวง ดินสอ", "เขตดุสิต กรุงเทพ", "10123"))
        var address2 = addressRepository.save(Address("239 มหาวิทยาลัยเชียงใหม่", "ต.สุเทพ", "อ.เมือง จ.เชียงใหม่", "50200"))
        var address3 = addressRepository.save(Address("ซักที่บนโลก", "ต.สุขสันต์", "อ.ในเมือง จ.ขอนแก่น", "12457"))

        var customer = customerRepository.save(Customer("Lung", "pm@go.th", UserStatus.ACTIVE))
        customer.defaultAddress = addressl

        var customer1 = customerRepository.save(Customer("ชัชชาติ", "chut@taopoon.com", UserStatus.ACTIVE))
        customer1.defaultAddress = address2

        var customer2 = customerRepository.save(Customer("ธนาธร", "thanathorn@life.com", UserStatus.PENDING))
        customer2.defaultAddress = address3

        var selectedProduct = selectedProductRepository.save(SelectedProduct(4))
        selectedProduct.product = product2

        var selectedProduct1 = selectedProductRepository.save(SelectedProduct(1))
        selectedProduct1.product = product4

        var selectedProduct2 = selectedProductRepository.save(SelectedProduct(1))
        selectedProduct2.product = productl

        var selectedProduct3 = selectedProductRepository.save(SelectedProduct(1))
        selectedProduct3.product = product3


        var selectedProduct5 = selectedProductRepository.save(SelectedProduct(1))
        selectedProduct5.product = product4

        var shoppingCart = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.SENT))
        shoppingCart.Customer = customer
        shoppingCart.SelectedProduct.add(selectedProduct)
        shoppingCart.SelectedProduct.add(selectedProduct1)

        var shoppingCart1 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.WAIT))
        shoppingCart1.Customer = customer1
        shoppingCart1.SelectedProduct.add(selectedProduct5)
        shoppingCart1.SelectedProduct.add(selectedProduct2)
        shoppingCart1.SelectedProduct.add(selectedProduct3)


        dataLoader.loadData()


    }


}