package dv.spring592110131.demo.util


import dv.spring592110131.demo.entity.Address
import dv.spring592110131.demo.entity.Manufacturer
import dv.spring592110131.demo.entity.Product

import dv.spring592110131.demo.entity.ShoppingCart
import dv.spring592110131.demo.entity.dto.AddressDto
import dv.spring592110131.demo.entity.dto.ManufacturerDto
import dv.spring592110131.demo.entity.dto.ProductDto

import dv.spring592110131.demo.entity.dto.ShoppingcartDto
import org.apache.commons.math3.analysis.function.Add
import org.mapstruct.InheritInverseConfiguration

import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }
    @Mappings(Mapping(source = "manufacturer",target = "manu"))
    fun mapProductDto(product: Product?): ProductDto?
    fun mapProductDto(product: List<Product>): List<ProductDto>



    @InheritInverseConfiguration
    fun mapManufacturer(manu: ManufacturerDto):Manufacturer

    fun mapManufacturer(manu:Manufacturer) : ManufacturerDto
    fun mapManufacturer(manu:List<Manufacturer>) : List<ManufacturerDto>

    @InheritInverseConfiguration
    fun mapAddress(address: AddressDto):Address

    fun mapAddress(address: Address):AddressDto
    fun mapAddress(address: List<Address>):List<Address>

    fun mapShoppingDto(shoppingCart: ShoppingCart):ShoppingcartDto
    fun mapShoppingDto(shoppingCart: List<ShoppingCart>):List<ShoppingcartDto>

    @InheritInverseConfiguration
    fun mapProductDto(productDto: ProductDto):Product




}