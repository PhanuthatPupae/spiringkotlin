//package dv.spring592110131.demo.controller
//
//import dv.spring592110131.demo.entity.Person
//import dv.spring592110131.demo.entity.myPerson
//import org.springframework.http.ResponseEntity
//import org.springframework.web.bind.annotation.*
//
//@RestController
//
//class HelloWorldController {
//    @GetMapping("/helloWorld")
//    fun getHelloWorld(): String {
//        return "HelloWorld"
//    }
//
//    @GetMapping("/test")
//    fun getTest(): String {
//        return "poopae"
//    }
//
//    @GetMapping("/person")
//    fun getPerson(): ResponseEntity<Any> {
//        val person = Person("somchai", "somrak", 15)
//        return ResponseEntity.ok(person)
//    }
//
//    @GetMapping("/myPerson")
//    fun getMyPerson(): ResponseEntity<Any> {
//        val myPerson = myPerson("poopae", "pae", 21)
//        return ResponseEntity.ok(myPerson)
//    }
//
//    @GetMapping("/persons")
//    fun getPersons(): ResponseEntity<Any> {
//        val person01 = Person("somchai", "somrak", 15)
//        val person02 = Person("Prayut", "Chan", 62)
//        val person03 = Person("Lung", "Pom", 65)
//        val persons = listOf<Person>(person01, person02, person03)
//        return ResponseEntity.ok(persons)
//
//    }
//
//    @GetMapping("/myPersons")
//    fun getMyPersons(): ResponseEntity<Any> {
//        val myPerson1 = myPerson("somchai", "somrak", 15)
//        val myPerson2 = myPerson("Prayut", "Chan", 62)
//        val myPerson3 = myPerson("Lung", "Pom", 65)
//        val myPersons = listOf<myPerson>(myPerson1, myPerson2, myPerson3)
//        return ResponseEntity.ok(myPersons)
//
//    }
//
//    @GetMapping("params")
//    fun getParams(@RequestParam("name") name: String, @RequestParam("surname") surname: String): ResponseEntity<Any> {
//        return ResponseEntity.ok("$name $surname")
//    }
//
//    @GetMapping("/params/{name}/{surname}/{age}")
//    fun getPathParams(@PathVariable("name") name: String,
//                      @PathVariable("surname") surname: String,
//                      @PathVariable("age") age: Int):
//
//            ResponseEntity<Any> {
//        val person = Person(name, surname, age)
//        return ResponseEntity.ok(person)
//    }
//    @PostMapping("/echo")
//    fun echo(@RequestBody person: Person): ResponseEntity<Any> {
//        return ResponseEntity.ok(person)
//
//    }
//}