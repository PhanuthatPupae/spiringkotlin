package dv.spring592110131.demo.controller

import dv.spring592110131.demo.service.ShoppingcartService
import dv.spring592110131.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ShippingController{
    @Autowired
    lateinit var shoppingcartService: ShoppingcartService
    @GetMapping("/shoppingcart")
    fun getShoppingcart ():ResponseEntity<Any>{
        val shoppingcarts = shoppingcartService.getShoppingcart()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingDto(shoppingcarts))
    }

}