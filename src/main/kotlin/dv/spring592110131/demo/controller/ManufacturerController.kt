package dv.spring592110131.demo.controller

import dv.spring592110131.demo.entity.Manufacturer
import dv.spring592110131.demo.entity.dto.ManufacturerDto
import dv.spring592110131.demo.service.ManufacturerService
import dv.spring592110131.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ManufacturerController {
    @Autowired
    lateinit var manufacturerService: ManufacturerService
    @GetMapping("/manufacturer")
    fun getAllManufacturer(): ResponseEntity<Any> {
        return  ResponseEntity.ok(manufacturerService.getManufacturers())

    }
    @PostMapping("/manufacturer")
    fun addManufacuturer(@RequestBody manu: ManufacturerDto):ResponseEntity<Any>{
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapManufacturer(manufacturerService.save(manu)))
    }
    @PutMapping("/manufacturer")
    fun updateManufacuturer(@PathVariable("manuId")id:Long?,
            @RequestBody manu:ManufacturerDto):ResponseEntity<Any>{
        manu.id== id

        return ResponseEntity.ok(MapperUtil.INSTANCE.mapManufacturer(manufacturerService.save(manu)))
    }
}