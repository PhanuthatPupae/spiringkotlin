package dv.spring592110131.demo.controller

import dv.spring592110131.demo.entity.dto.AddressDto
import dv.spring592110131.demo.entity.dto.ManufacturerDto
import dv.spring592110131.demo.service.AddressService
import dv.spring592110131.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
 class AddressController{
    @Autowired
    lateinit var addressService : AddressService
    @PostMapping("/address")
    fun addManufacuturer(@RequestBody add: AddressDto): ResponseEntity<Any> {
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapAddress(addressService.save(add)))
    }
    @GetMapping("/address")
    fun get():ResponseEntity<Any>{
        return ResponseEntity.ok(addressService.getAddress())
    }

    @PutMapping("/address/{addId}")
    fun update(@PathVariable("addId")id:Long?,@RequestBody add:AddressDto):ResponseEntity<Any>{
        add.id=id
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapAddress(addressService.save(add)))

    }

}
